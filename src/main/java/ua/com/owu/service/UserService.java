package ua.com.owu.service;

import ua.com.owu.entity.User;

/**
 * Created by Alex on 22.03.2017.
 */
public interface UserService {
    void save(User user);
    User findByName(String username);
}
