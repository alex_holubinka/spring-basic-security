package ua.com.owu.entity;

/**
 * Created by Alex on 22.03.2017.
 */
public enum Authority {
    ROLE_USER, ROLE_ADMIN;
}
