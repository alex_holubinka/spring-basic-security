<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Alex
  Date: 13.04.2017
  Time: 14:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Log In</title>
    <!-- <link href="<c:url value="/resources/css/LogInFormNew.css" />" rel="stylesheet"> -->
    <link href="<%=request.getContextPath()%>/resources/css/LogInFormNew.css" rel="stylesheet">
    <script src="<c:url value="/resources/js/LogInFormNew.js" />"></script>
</head>
<body>

<c:if test="${not empty error}">
    ${error}
</c:if>

<div class="container">
    <section id="content">
        <form name='form_login' action="<c:url value='/j_spring_security_check' />" method='POST'>
            <h1>Вхід у систему</h1>
            <div>
                <input type="text" name="user_login" placeholder="Username" required="" id="username" />
            </div>
            <div>
                <input type="password" name="password_login" placeholder="Password" required="" id="password" />
            </div>
            <div>
                <input type="submit" value="Вхід" />
                <div>
                    <a href="#">Забули пароль?</a>
                    <a href="#">Реєстрація</a><br>
                    <div class="divrem">
                        <div>
                            <label><input name="remember-me" type="checkbox"/><span>Запам'ятати</span></label>
                        </div>
                    </div>
                </div>



            </div>
            <input type="hidden" name="${_csrf.parameterName}"
                   value="${_csrf.token}" />

        </form>
    </section>
</div>
</body>
</html>
